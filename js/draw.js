function drawInscribedPoly(context, x, y, circleRadius, sides) {
  if (sides < 3) return
  context.beginPath()
  var a = ((Math.PI * 2)/sides)
  context.translate(x,y)
  context.moveTo(circleRadius,0)
  for (var i = 1; i < sides; i++) {
    context.lineTo(
      circleRadius*Math.cos(a*i),
      circleRadius*Math.sin(a*i)
    )
  }
  context.closePath()
}

function drawInscribedPolyFactory(context) {
  return drawInscribedPoly.bind(null, context)
}

let myCanvas = document.querySelector('#workspace')
let context = myCanvas.getContext('2d')
context.fillStyle = 'green'
let myDrawPoly = drawInscribedPolyFactory(context)

myDrawPoly(100, 100, 30, 6)
context.fill()
context.stroke()

context.fillStyle = 'red'
myDrawPoly(
  30*Math.cos(Math.PI * 2/6) * 3,
  30*Math.sin(Math.PI * 2/6),
  30,
  6
)
context.fill()
context.stroke()


context.fillStyle = 'blue'
myDrawPoly(
  30*Math.cos(Math.PI * 2/6) * 3,
  30*Math.sin(Math.PI * 2/6),
  30,
  6
)
context.fill()
context.stroke()
